<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "������������ ���������� ����������������� �����. ����������������� ������� �������������");
$APPLICATION->SetPageProperty("keywords", "������������, ������������ ����������, ������������ �����������������");
$APPLICATION->SetTitle("");

?><div class="main-banner-wrap">
	<div class="container">
		<div class="main-banner-group">
			<div class="main-banner-bl">
 <img src="/bitrix/templates/cherry/img/main-banner.jpg" alt="">
				<div class="main-banner-info">
					<div>
						 ����������
					</div>
					<div>
						 ���������������
					</div>
					<div>
						 �����������������
					</div>
					<div>
						 ���� - 4 ���� 01
					</div>
					<div>
						 <!--������ � � ������������-->
					</div>
				</div>
				<div class="main-banner-product-img">
 <img src="/bitrix/templates/cherry/img/banner-product.jpg" alt="">
				</div>
			</div>
			<div class="main-banner-advantages">
				<div class="advantages-list-tt">
					 ������������
				</div>
				<ul class="adv-list">
					<li><i class="adv-list-icon"></i>�������� �������������</li>
					<li><i class="adv-list-icon"></i>������� �������� ������������</li>
					<li><i class="adv-list-icon"></i>������������</li>
					<li><i class="adv-list-icon"></i>������� �������������</li>
					<li><i class="adv-list-icon"></i>������ ���� ������������</li>
					<li><i class="adv-list-icon"></i>������������ ��� ����������</li>
					<li><i class="adv-list-icon"></i>��������� �� ������</li>
				</ul>
 <a href="/cherry/main/about-product/" class="btn">���������</a>
			</div>
		</div>
	</div>
</div>
<div class="help-for-wrap">
	<div class="container">
		<div class="help-for-bl">
			<div class="help-for-item">
				<div class="help-for-text">
 <i class="help-for-icon help-for-icon-1"></i>
					<div>
						 ���
					</div>
					<div>
						 ����� ���������
					</div>
 <a href="/cherry/main/how-to-use/#block1">���������</a>
				</div>
			</div>
			<div class="help-for-item">
				<div class="help-for-text">
 <i class="help-for-icon help-for-icon-2"></i>
					<div>
						 ���
					</div>
					<div>
						 �������
					</div>
 <a href="/cherry/main/how-to-use/#block2">���������</a>
				</div>
			</div>
			<div class="help-for-item">
				<div class="help-for-text">
 <i class="help-for-icon help-for-icon-3"></i>
					<div>
						 ���
					</div>
					<div>
						 ����������
					</div>
 <a href="/cherry/main/how-to-use/#block3">���������</a>
				</div>
			</div>
			<div class="help-for-item">
				<div class="help-for-text">
 <i class="help-for-icon help-for-icon-4"></i>
					<div>
						 ���
					</div>
					<div>
						 �������� ������
					</div>
 <a href="/cherry/main/how-to-use/#block4">���������</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="instruction-wrap">
		<div class="instr-bl">
			<p>
 <span class="instr-color">��� ������� ������ ������������� </span>������������ ������� ��������� � ���� ����������. ��������� ���������� ���������� � ������� 1 ��� �� ������� �������� � ��������.<br>
 <br>
			</p>
			<div class="instr-img">
 <img src="/bitrix/templates/cherry/img/instruction-1.jpg" alt=""> <span class="instr-img-text instr-img-text-1">������������ ����� - ����������� ��������� �������� ������������� �� ������� ��������. ����� ������� ����� ������������ ��� ��� ����������� ������ � �������.</span>
			</div>
		</div>
		<div class="instr-bl">
			<p>
 <span class="instr-color">��� ��������������� ������ �������������</span> ������� ����������� ������������ � ������������� ����. ��������� ���������� ���������� � ������� 1 ��� �� ������� �������� � ��������.
			</p>
			<div class="instr-img">
 <img src="/bitrix/templates/cherry/img/instruction-2.jpg" alt=""> <span class="instr-img-text instr-img-text-2">����� �������� ��������, ����������� ��������, �� ������� �������� ������ � �����. ����������� ������� ������������ ������ �� �����������������, �� ������ ���� ��������� ��������� � ���������.</span>
			</div>
		</div>
	</div>
</div>
<div class="about-company-wrap dark-section-bl">
	<div class="container">
 <span class="title-site title-site_h1">� ��������</span>
		<div class="about-company-info">
			<div>
				<p>
					 �������� "������ ������" �������� �� ����� � 2011 ���� � ������������� �������������� ������ � ������� �������� ������������. �� ���� ������ �� ����������� ��� ����� ������������� �������� ����� 100 �������� ��������� ��������� �� ������, ���������� ���, ������� ��������� ����� � ������.
				</p>
				<p>
					 ����������� �������������� �������� �������� ���������������� (����. ����. ����), ������� �������� ��� �������� � �������� ������ �� ����������� � ����������������������, �������� �������� �������-����������� ���������� � �������� �������������� ��������. 
				</p>
			</div>
			<div>
				<p>
�������� ���������� � ��������� ��������������� ��������� � �� ����� ������ ��� ������ � ������ �� ���������� ������� ��������������� ������� � ������� ��� ������.</p>
				<p>
					 ����������� ���������� ������ ��������� ��������� � ���������� ������������ ��������������� ���������� ������������ ���������������� ����������� ������������������ ���� ������������������ ������ �� � 2603573 �� ���� "�����". ����������� ������� ������ ��� ����������� ���������, � ��� ����� �� ��������� ���, ����� �� ���� ��������� ������������ �������.
				</p>
			</div>
		</div>
	</div>
</div>
<div class="buy-product-wrap">
	<div class="container">
 <span class=" title-site title-site_h1">������ ���� �����</span>
		<div class="buy-product-group">
			<div class="buy-product-img">
<div class="owl-slider-main owl-carousel">
    
    <div class="product-list-wrapp">
        <img width="360" alt="Photo-Box-WEB-4.jpg" src="/upload/medialibrary/766/766e98effce5cfff32268377400ab91c.jpg" height="380" title="������� �����">
    </div>
    
    <div class="product-list-wraps">
        <img width="360" alt="Photo-Vishnya-WEB-4.jpg" src="/upload/medialibrary/fdc/fdcc52c8971b4a025b81c4a4943d3781.jpg" height="380" title="�����">
    </div>
</div>
<div class="product-list-wrapp">
    <div class="product-list owl-thumbs owl-carousel">
        <div class="product-list-point">
            <img src="/upload/medialibrary/766/766e98effce5cfff32268377400ab91c.jpg" alt="">
        </div>
        <div class="product-list-point">
            <img src="/upload/medialibrary/fdc/fdcc52c8971b4a025b81c4a4943d3781.jpg" alt="">
        </div>
    </div>
</div>				
			</div>
			<div class="buy-product-info-wrap">
				<div class="buy-product-info">
					<div class="buy-pr-name">
						 ������������ �����
					</div>
					<div>
						 ������ ������ ����������: ����
					</div>
					<div>
						 ����� �������: �� 36 ��� �
					</div>
					<div>
						 ��� ������������: 3,2 ��.
					</div>
					<div>
						 ������������������ ��������: 200�200 ��.
					</div>
					<div>
						 ������� ������������: 180 ��.
					</div>
				</div>
				<div class="buy-product-in-stock">
					 � �������
				</div>
				<div class="buy-product-price">
					 3940 ���
				</div>
				<div class="buy-bl-btn">
 <a href="/cherry/main/order/" class="btn btn-fill">������</a> <a href="#request-form" class="btn open-modal">������ ������</a>
				</div>
			</div>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>