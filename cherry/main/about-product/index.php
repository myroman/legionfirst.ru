<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("");

?><div class="container">
	<div class="about-product-text">
		<div class="sale-product-bl">
 <span class="title-site title-site_h1">������������ �����</span> <img src="/upload/medialibrary/766/766e98effce5cfff32268377400ab91c.jpg" alt="">
			<div class="sale-price">
				3940 ���
			</div>
 <a href="/cherry/main/order/" class="btn btn-fill btn-small">������</a>
		</div>
<h1 class=�info-heading">���������� ����������������� ������������ �����</h1>
		<p>
			����ߠ- ��� ��������������� ���������� ����������������������������� ������ ���������. <span class="text-color">������� ��������</span> ����������� � ������������� ����������� �� ������� ����������� ������� � ������������� �� ��� ���������� ����� �� ��������� ������� � ����������� �������������� ������������ ������ ���������������� ������, ��� �������� � �������� ������� ������������ � ������������ ����������������������� ��������� �� ������� ��� ������ �������.
		</p>
		<p>
			<span class="text-color">����� ����������:</span> ��������, ��������, ����������, ����, ������ �������, ������� ��������� (�������������������� ���������,������,�������� �������� � ������������,������� �������������� ��������),�������, ������, ��������� ���������, ��������, ����� (�������������� � ������ �������� � ������������� ��������������������� � ������� �������), ��������� ����� �����,������ ���������� ������������ � ��������� �����,��������� �����,�������������� ������,�������������. ��������� � ���������� ����� ������ <a href="#">������</a>
		</p>
	</div>
</div>
<div class="dark-section-bl about-product-info">
	<div class="container">
 <span class="title-site title-site_h1">����������� ��������������</span>
		<div class="about-prod-info-bl">
			<div class="about-prod-info-item">
 <i class="info-item-icon info-item-icon-1"></i> <span class="info-item-info">����</span>
				������ ������ ����������
			</div>
			<div class="about-prod-info-item">
 <i class="info-item-icon info-item-icon-2"></i> <span class="info-item-info">�� 36 ��� �</span>
				����� �������
			</div>
			<div class="about-prod-info-item">
 <i class="info-item-icon info-item-icon-3"></i> <span class="info-item-info">3,2 ��</span>
				��� ������������
			</div>
			<div class="about-prod-info-item">
 <i class="info-item-icon info-item-icon-4"></i> <span class="info-item-info">180 ��</span>
				������� ������������
			</div>
		</div>
	</div>
</div>
<div class="main-advantages-wrap">
	<div class="container">
 <span class="title-site title-site_h1">�������� ������������</span>
		<div class="main-adv-bl">
			<div class="main-adv-item">
 <i class="main-adv-icon"></i>
				<div class="text-color">
					�������� �������������
				</div>
				 ����� ������� ��� ����������� ������ � ������� ����� ���������� ������������ ������������ �����. ���������� ��������� ��� � ���� ���������� � ��� ����� 1 ���. ����� �������� � �������� �� ��������� ��������������.
			</div>
			<div class="main-adv-item">
 <i class="main-adv-icon"></i>
				<div class="text-color">
					������������ ��� ����������
				</div>
				  ������������ ����� �� ��������� ����� �������� �����, ��������� � ���������� �����. ���������� ������������ ������� �� ������ ����������, ������� �� ���������, ���� ���� ��������� � ���������������� �������� �� ���.
			</div>
			<div class="main-adv-item">
 <i class="main-adv-icon"></i>
				<div class="text-color">
					������� �������� ������������
				</div>
				 � ������� ������� ������� ��������� ������� � ��� ����� ����������� �������������� ������������ ���������� � ������������� ��� ���������������.
			</div>
			<div class="main-adv-item">
 <i class="main-adv-icon"></i>
				<div class="text-color">
					������ ���� ������������
				</div>
 ��� �� ����������� ������������ ������������ ����� � ����������� ����� ������ ��������, ����� ����������� ��� ����������������� � ������� 5 ���.
			</div>
			<div class="main-adv-item">
 <i class="main-adv-icon"></i>
				<div class="text-color">
					������������
				</div>
				 ������������ ����� ��������� �������������. ���������� ������������ � ����� ������������� ����� � ����� ��������, �� ��������� �� �������� � ��������.
			</div>
			<div class="main-adv-item">
 <i class="main-adv-icon"></i>
				<div class="text-color">
					������������� ������� ������������� � ��������� � ������
				</div>
				 ������� ������������ ����������� �� ��������� ��� � ��� �������� ������ �� ������������������ ���������
			</div>
			<div class="main-adv-item">
 <i class="main-adv-icon"></i>
				<div class="text-color">
					������� �������������
				</div>
				 ������������ ����� �������� ������������ ���������� � ��������� ������� 36 �.���. ��������, ������� �������� 12 ��.�. � � ������� ������� 3 �. ���� ���������� ����������� ���� ��� � ����������� ���������.
			</div>
		</div>
	</div>
</div>
<div class="documents-wrap dark-section-bl">
	<div class="container">
 <span class="title-site title-site_h1">���������</span>
		<div class="documents-bl">
			<div class="documents-item">
 <a href="/upload/docs/cert-powder.pdf" download="���������� ������������ �� ����������� �������"> <i class="documents-icon"></i>
				���������� ������������ �� ����������� ������� </a>
			</div>
			<div class="documents-item">
 <a href="/upload/docs/cert-vishnya.pdf" download="���������� ������������ �� ����������"> <i class="documents-icon"></i>
				���������� ������������ �� ����������</a>
			</div>
			<div class="documents-item">
 <a href="/upload/docs/passport-vishnya.pdf" download="������� �� ���������� ���� �����"> <i class="documents-icon"></i>
				������� �� ���������� ���� �����</a>
			</div>
			<div class="documents-item">
 <a href="/upload/docs/diplom.jpg" download="������ � ������ ��������� �������� � ������������"> <i class="documents-icon"></i>
				������ � ������ ��������� �������� � ������������</a>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="presentation-bl">
		<div>
			 <iframe width="100%" height="100%" src="https://www.youtube.com/embed/0_sgdTZQigM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		</div>
	</div>
</div><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>