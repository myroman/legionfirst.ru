<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("");

?><div class="how-to-use-wrapper">
	<div class="container">
		<div class="sale-product-bl">
 <span class=" title-site title-site_h1">������������ �����</span> <img src="/upload/medialibrary/766/766e98effce5cfff32268377400ab91c.jpg" alt="" style="width: 170px;">
			<div class="sale-price">
				 3940 ���
			</div>
 <a href="/cherry/main/order/" class="btn btn-fill btn-small">������</a>
		</div>
		<div class="text-bl">
			<p>
				 ���Ѡ����� ������������ ��� ������������� � �������� ���������� �������� ������� ������ ������������� ������� ������� (����� �), ������ ������� ���������� (����� �),������������� ������� (����� �) � ������������������� (����� �), ������������ ��� �����������.
			</p>
			<p>
				 ������������ ����� ����� ���� ��������� ��� � ������, ��� � � �������������� ������.<br><span class="text-color">��� ������� ������ </span> ������������� ������������ ������� ��������� � ���� ����������.<br><span class="text-color">��� ��������������� ������</span>
				������������� ������� ����������� ������������ � ������������� ����. � ����� ������� ��������� ���������� ���������� � ������� 1 ��� �� ������� �������� � ��������.
			</p>
			<p>
				<br>
				<br>
				<br>
				<br>
			</p>
		</div>
	</div>
	<div class="dark-section-bl how-to-use-bl">
		<div class="container">
			<div class="how-to-use-text">
				<div class="text-color">
					 <a name="block1"></a>��� ����� ����� � ���������
				</div>
				<p>
					 ������� ������� ������������ ��������� ������� ������������������������� ������������� � ����������.�������, � ����������� �� ��� ���� ��� ������� ������� ������ �� ������. ��� �������� � ���������� �����, ��� ������ �� 2017 ��� � ������ ���������92 929�������� � ����� ������������� ������ ���.
				</p>
				<p>
					 ����������� ���������� ���������� ������ �����, ����� ������������ ����������� ��������� ������ � ��������� �����, �� ������� ��������. �� ���� ��� ���� ������, ������� � ������������ ����, ������ �������� 2-3 ����� � ������� ���, ��� ��������� ��� ����������� �������� ��������������� ������.�� ������ ������, ����� ������� ���������� ������, ����������������� ������������ �������� ����� ����� � ���������� ���� � ����� �� �������� ���������. �������� �������, ������� � ������������ ��������� ���� ����, ����� ����� ������������ ����� ��� ���������� ���������� ����.
				</p>
				<p>
					 ����������������� ������������ ��� �� ����� ����������� �������� ���������� ������������� ��� ������� ��������������� �����, ��� ����� ��� ����������� ����� � ����� ������ �� ����� ������ ��������� ������� �������������, ����� ���� �� �����.
				</p>
			</div>
			<div class="how-to-use-img-wrapper">
				<div class="how-to-use-img">
 <img src="/bitrix/templates/cherry/img/instruction-3.jpg" alt="">
				</div>
				<div class="how-to-use-img">
 <img src="/bitrix/templates/cherry/img/instruction-4.jpg" alt="">
				</div>
			</div>
		</div>
	</div>
	<div class="how-to-use-bl how-to-use-bl-reverse">
		<div class="container">
			<div class="how-to-use-text">
				<div class="text-color">
					 <a name="block2"></a>��� ��������� ���������
				</div>
				<p>
					 ��� ������-����������� �������� ���������� ���������� ���������� � ������� ���������� ��� ������� �����.����������� �������, ������� �������� � ���� ��������� ���� ��� ������� ���������, �������� �� ���� �����, ����� ������������ ����������. ���� ��� �� ��������� ��� ���� ��������� �������� � ����, � ��� ������������� ������������ ������. ����� ���������� ��������, ������������� � �������������� ������� ������������� �� ���� �� �������
				</p>
				<p>
					 ����������������� ������������ ����� �������� ��������, ������� ��������� ��������, �� ���������� �������� ������ � �����.
				</p>
				<p>
					 ������������ ����� ������������ ��� ��������� � ������� ������� � �������� 30 ���.�. �� ������. ��� � ���� ����� � ������� �������������� � ��������� ����������� ������ ����. ��������� ��������� ���� ���������� ����, ������� �������� ����������� �������� ������������� ����������, �� ������ ���� ��������� ���������. ����� ���������� ������ ����� ���������� ������ �������� ��������������
				</p>
			</div>
			<div class="how-to-use-img-wrapper">
				<div class="how-to-use-img">
 <img src="/bitrix/templates/cherry/img/instruction-5.jpg" alt="">
				</div>
				<div class="how-to-use-img">
 <img src="/bitrix/templates/cherry/img/instruction-6.jpg" alt="">
				</div>
			</div>
		</div>
	</div>
	<div class="dark-section-bl how-to-use-bl">
		<div class="container">
			<div class="how-to-use-text">
				<div class="text-color">
					 <a name="block3"></a>��� ����������
				</div>
				<p>
					 ���� ����� ����� ���������� ����������� �� �������, ��������������� � ������������� ���������� ��� ��������� �������� �������������.
				</p>
				<p>
					 ����������� ���������� ������������������ ������������ ��� ���������� ����� ����� ���� ������������ ������� �� ���������. ������� ���Ѡ� ���� ������ - ��� ����� ������� � ������� �������.�
				</p>
				<p>
					 ����� �������� ������������ (��������������) ������������� ������������ ����� �� ����������, ��� ������� ���������� ���������� ������������������� �������� ������� ��� ���������� ������.
				</p>
			</div>
			<div class="how-to-use-img-wrapper">
				<div class="how-to-use-img">
 <img src="/bitrix/templates/cherry/img/instruction-7.jpg" alt="">
				</div>
			</div>
		</div>
	</div>
	<div class="how-to-use-bl how-to-use-bl-reverse">
		<div class="container">
			<div class="how-to-use-text">
				<div class="text-color">
					 <a name="block4"></a>��� �������� ������
				</div>
				<p>
					 ������������ ����� � ������������ �������� ������� ������� �������� �������� ������� ������ � �����.������ ���������� � ���� ������, �������� ������� ������ ����� ����� � ���������� ���� � ����� �� �������� ���������.
				</p>
				<p>
					 ��������� ���� ����� �������� ��������� ������� �������������, ��� ��� ������ ����������� ��������� ��� �������� �������� ������ � ���������� ������� ������������� �������.
				</p>
			</div>
			<div class="how-to-use-img-wrapper">
				<div class="how-to-use-img">
 <img src="/bitrix/templates/cherry/img/instruction-8.jpg" alt="">
				</div>
			</div>
		</div>
	</div>
</div>
<br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>