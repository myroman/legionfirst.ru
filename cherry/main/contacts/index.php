<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("");

?><style rel="stylesheet">
    .requisites {
        margin-top: 30px;
    }

    .requisites table {
        width: 100%;
        border-collapse: collapse;
    }

    .requisites td {
        padding: 8px;
    }

    .requisites>h2 {
        margin-bottom: 8px;
    }

    .requisites tr:nth-child(odd) {
        background: #f6f6f6;
    }
</style>
<div class="contacts-wrapper">
	<div class="container">
		<div class="sale-product-bl">
 <span class=" title-site title-site_h1">������������ �����</span> <img src="/upload/medialibrary/766/766e98effce5cfff32268377400ab91c.jpg" alt="">
			<div class="sale-price">
				 3940 ���
			</div>
 <a href="/cherry/main/order/" class="btn btn-fill btn-small">������</a>
		</div>
		<div class="question-contacts">
			<div>
				 ���� � ��� �������� �����-���� �������, �������� ��� <a href="#request-form" class="btn open-modal">������ ������</a>
			</div>
			<div>
				 ������� ������ �� ��������: 8 800 500-67-46 (������ ����������)
			</div>
		</div>
		<div class="contacts-info">
			<div>
				 ����������� ����:
			</div>
			<div>
				 192012, �����-���������, �������� ���������� �������, 120�
			</div>
			<div>
				 ���: +7 (812) 407-38-41
			</div>
			<div>
				 e-mail: <a href="mailto:info@legionfirst.ru">info@legionfirst.ru</a> <br>
			</div>
			<div>
				 <?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view",
	"",
	Array(
		"CONTROLS" => array("ZOOM","MINIMAP","TYPECONTROL","SCALELINE"),
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:59.86478967431763;s:10:\"yandex_lon\";d:30.474190981475477;s:12:\"yandex_scale\";i:15;s:10:\"PLACEMARKS\";a:2:{i:0;a:3:{s:3:\"LON\";d:30.475006373016;s:3:\"LAT\";d:59.863688978294;s:4:\"TEXT\";s:0:\"\";}i:1;a:3:{s:3:\"LON\";d:30.475006373016;s:3:\"LAT\";d:59.863688978294;s:4:\"TEXT\";s:52:\"����� ���������, ��. ���������� �������, 120, ���. �\";}}}",
		"MAP_HEIGHT" => "400",
		"MAP_ID" => "",
		"MAP_WIDTH" => "600",
		"OPTIONS" => array("ENABLE_SCROLL_ZOOM","ENABLE_DBLCLICK_ZOOM","ENABLE_DRAGGING")
	)
);?><br>
			</div>

<div class="requisites">
    <h2>��������� ��� ����������� ���</h2>
    <table>
        <thead>
            <th width="30%"></th>
            <th width="70%"></th>
        </thead>
        <tbody>
            <tr>
                <td>������ ������������ �����������</td>
                <td>�������� � ������������ ���������������� ������� �������</td>
            </tr>
            <tr>
                <td>����������� �������� �����������</td>
                <td>��� ������� �������</td>
            </tr>
            <tr>
                <td>����������� �����</td>
                <td>191014, �. �����-���������, ��. ��������, 26</td>
            </tr>
            <tr>
                <td>����������� �����</td>
<td>192012, �. ����� � ���������, ��. ���������� �������, �. 120 ���. �, <br /> �� ���������������, ���� 209 </td>
</tr>
            <tr>
                <td>���/���</td>
                <td>7842447593/784201001</td>
            </tr>
            <tr>
                <td>����</td>
                <td>1117847062970</td>
            </tr>
            <tr>
                <td>��� �� ����</td>
                <td>90774395</td>
            </tr>
            <tr>
                <td>����������� ��������</td>
                <td>�������� �.�.</td>
            </tr>
            <tr>
                <td>�������</td>
                <td>8 981 896 28 02</td>
            </tr>
<tr>
                <td>e-mail</td>
                <td>info@legionfirst.ru</td>
            </tr>
            <tr>
                <td>���������� ���������</td>
                <td>������-�������� ���� ��� ��������, ��� 044030653, <br />
                    �/��: � 40702810755100185826, �/� 30101810500000000653</td>
            </tr>
        </tbody>
    </table>
</div>
<div class="requisites">
    <h2>��������� ��� ���������� ���</h2>
    <table>
        <thead>
            <th width="30%"></th>
            <th width="70%"></th>
        </thead>
        <tbody>
            <tr>
                <td>��</td>
                <td>��������� �.�.</td>
            </tr>
            <tr>
                <td>���</td>
                <td>026508954586</td>
            </tr>
            <tr>
                <td>������</td>
                <td>316784700324106</td>
            </tr>
            <tr>
                <td>����������� �����</td>
                <td>192012, �. �����-���������, ��. ���������� ������� 138/2</td>
            </tr>
    <tr>
                <td>����������� �����</td>
                <td>192012, �. �����-���������, ��. ���������� ������� 138/2</td>
            </tr>
 <tr>
                <td>�������</td>
<td>8 921 896 17 86</td>
</tr>
 <tr>
                <td>e-mail</td>
<td>zakaz-opps@yandex.ru</td>
</tr>
            <tr>
                <td>���������� ���������</td>
                <td>������ "�����-�������������" �� "�����-���ʔ, ���: 044030786, <br />
                    �/��: � 40802810332130002235</td>
            </tr>
        </tbody>
    </table>
</div>

		</div>
		 <!--<img src="<?=SITE_TEMPLATE_PATH?>/img/map.jpg" alt="">-->
	</div>
</div>
 <br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>