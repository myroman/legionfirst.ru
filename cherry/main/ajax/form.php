<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<?

CModule::IncludeModule("form");
if (isset($_POST["action"]) && $_POST["action"] == "feedback") {

    $FORM_ID = 2;

    // ������ �������� �������
    $arValues = array(
        "form_text_6" => iconv("UTF-8","windows-1251",$_POST["name"]),
        "form_text_7" =>  iconv("UTF-8","windows-1251",$_POST["phone"]),
        "form_text_8" =>  iconv("UTF-8","windows-1251",$_POST["email"]),
        "form_textarea_606" =>  iconv("UTF-8","windows-1251",$_POST["message"]),
    );

    // �������� ����� ���������
    if ($RESULT_ID = CFormResult::Add($FORM_ID, $arValues)) {
        CFormCRM::onResultAdded($FORM_ID, $RESULT_ID);
        CFormResult::SetEvent($RESULT_ID);
        CFormResult::Mail($RESULT_ID);

        echo json_encode(array("success" => "ok"));

    }
    else {
        global $strError;
        echo json_encode(array("error" => $strError));
    }
    die();
}

if (isset($_POST["action"]) && $_POST["action"] == "order") {

    $FORM_ID = 3;

    // ������ �������� �������
    $arValues = array(
        "form_text_10" => iconv("UTF-8","windows-1251",$_POST["name"]),
        "form_text_11" =>  iconv("UTF-8","windows-1251",$_POST["phone"]),
        "form_text_12" =>  iconv("UTF-8","windows-1251",$_POST["email"]),
        "form_radio_SIMPLE_QUESTION_667" => ($_POST["person"]),
        "form_text_15" =>  iconv("UTF-8","windows-1251",$_POST["country"]),
        "form_text_16" =>  iconv("UTF-8","windows-1251",$_POST["city"]),
        "form_textarea_17" =>  iconv("UTF-8","windows-1251",$_POST["message"]),
        "form_text_18" =>  iconv("UTF-8","windows-1251",$_POST["quantity"]),
    );

    // �������� ����� ���������
    if ($RESULT_ID = CFormResult::Add($FORM_ID, $arValues)) {
        CFormCRM::onResultAdded($FORM_ID, $RESULT_ID);
        CFormResult::SetEvent($RESULT_ID);
        CFormResult::Mail($RESULT_ID);

        echo json_encode(array("success" => "ok"));

    }
    else {
        global $strError;
        echo json_encode(array("error" => $strError));
    }
    die();
}