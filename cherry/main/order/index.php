<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("");

?>    <div class="container">
        <div class="order-wrap">

            <span class=" title-site title-site_h1">���������� ������</span>
            <div class="order-table-wrap">

                <div class="order-prod-name order-prod-bl">
                    <div class="order-prod-inner-tt">������������</div>
                    <div class="order-prod-info">
                        <div class="order-prod-info-img"><img src="/upload/medialibrary/766/766e98effce5cfff32268377400ab91c.jpg" alt=""></div>
                        <div class="order-prod-info-text">
                            <div class="buy-pr-name">������������ �����</div>
                            <div>������ ������ ����������: ����</div>
                            <div>����� �������: �� 36 ��� �</div>
                            <div>��� ������������: 3,2 ��.</div>
                            <div>������������������ ��������: 200�200 ��.</div>
                            <div>������� ������������: 180 ��.</div>
                        </div>
                    </div>
                </div>

                <div class="order-prod-price order-prod-bl">
                    <div class="order-prod-inner-tt">����</div>
                    <div class="text_bold order-prod-text">3940 ���</div>
                </div>

                <div class="order-prod-bl order-prod-number">
                    <div class="order-prod-inner-tt">����������</div>
                    <div class="order-prod-number-group order-prod-text">
                        <span id="quantity-minus" class="text_bold">-</span>
                        <span id="quantity" class="text_bold">1</span>
                        <span id="quantity-plus" class="text_bold">+</span>
                    </div>
                </div>

                <div class="order-prod-bl">
                    <div class="order-prod-inner-tt">�����</div>
                    <div class="text_bold order-prod-text"><span id="full_price">3940</span> ���</div>
                </div>
            </div>


            <div class="order-form-wrap">
                <div class="corner-border-line">��������� �����, ����� ����� ���� ��������� �������� � ����, �����
                    �������� �������� ������ � ��������
                </div>

                <form action="" class="order-form" id="order-data-form">
                    <div class="order-form-group">
                        <div class="order-form-tt">���� ������</div>
                        <div class="form-item order-form-name">
                            <label for="order-name">���� ���</label>
                            <input type="text" name="name" placeholder="��� �������" id="order-name">
                        </div>

                        <div class="order-form-safe-group">
                            <div class="order-form-safe-item-wrap">
                                <div class="form-item">
                                    <label for="order-telephone">�������</label>
                                    <input type="text" name="phone" placeholder="+7 (___) ___-__-__" id="order-telephone" class="mask-phone">
                                </div>

                                <div class="form-item">
                                    <label for="order-email">E-mail:</label>
                                    <input type="text" name="email" placeholder="example@mail.ru" id="order-email">
                                </div>
                            </div>

                            <div class="order-form-safe-info">
                                <i class="safe-icon"></i>
                                <span>����������� ������������ ����� ������</span>
                            </div>
                        </div>

                        <div class="form-radio">
                            <div>
                                <input type="radio" checked name="person" value="13" id="order-person-1">
                                <label for="order-person-1">���������� ����</label>
                            </div>
                            <div>
                                <input type="radio" name="person" value="14" id="order-person-2">
                                <label for="order-person-2">����������� ����</label>
                            </div>
                        </div>
                    </div>


                    <div class="order-form-group">
                        <div class="order-form-tt">���� ������</div>
                        <div class="form-item">
                            <label for="country">������:</label>
                            <select name="country" id="country">
                                <option value="������">������</option>
                                <option value="�������">�������</option>
                                <option value="����������">����������</option>
                            </select>
                        </div>

                        <div class="form-item">
                            <label for="city">�����:</label>
                            <input type="text" name="city" id="city" placeholder="�����-���������">
                        </div>
                    </div>

                    <textarea name="message" placeholder="����������� � ������"></textarea>
                    <div class="order-form-checkbox">
                        <input type="checkbox" id="order-agreement">
                        <label for="order-agreement">� �������� �� <span class="text-color">��������� ������������ ������</span></label>
                    </div>
                    <input type="hidden" name="full-price" value="3940">
                    <input type="hidden" name="quantity" value="1">
                    <input type="hidden" name="action" value="order">
                    <button class="btn btn-fill">�������� �����</button>
            </div>

            </form>
        </div>


    </div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>